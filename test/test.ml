open Yojson.Basic
open Yojson.Basic.Util

let get_blocks_from_json block_dict reference =
  (* Get the encoded blocks from a json string *)
  block_dict |> member reference |> to_string

let get_dict_from_json json = 
  (* Get the dict (string * string) list from a json string *)
  let block_dict = json |> member "blocks" in
  let references = block_dict |> keys in
  let blocks = List.map (get_blocks_from_json block_dict) references in
  let decoded_references = List.map Base32.decode_exn references in
  let decoded_blocks = List.map Base32.decode_exn blocks in
  List.combine decoded_references decoded_blocks

let prepare_dict_for_json dict = 
  (* Receive a dict `(string * string) list` and make it json-compatible for Yojson *)
  let f (k, v) = ((Base32.encode_exn ~pad:false k), `String (Base32.encode_exn ~pad:false v)) in
  List.map f dict	

let create_json id name description content convergence_secret block_size level root_reference root_key urn dict =
  (* Create the json string that will be used to save an encryption *)
  `Assoc [
    ("id", `Int id);
    ("name", `String name);
    ("description", `String description);
    ("content", `String content);
    ("convergence-secret", `String convergence_secret);
    ("block-size", `Int block_size);
    ("read-capability", `Assoc [
        ("block-size", `Int block_size);
        ("level", `Int level);
        ("root-reference", `String root_reference);
        ("root-key", `String root_key)
      ]);
    ("urn", `String urn);
    ("blocks", `Assoc dict);
  ]

let encode_to_json ?(name = "no name") ?(id = 0) ?(description = "no description") content convergence_secret block_size =
 (*
	Encode a string of content with Eris and outputs a json string according to the format of test-vectors (in the test-vector folder)
	Name, id and description are optional arguments, and will be used in the json string
	Content, convergence_secret and block_size are required for the encoding
	*)
  let dict, r_c, urn = Eris.encode_urn ~convergence_secret ~block_size content in
  let dict = prepare_dict_for_json dict in
  let b32 = Base32.encode_exn ~pad:false in
  let json = create_json id name description (b32  content) (b32 convergence_secret) r_c.block_size r_c.level (b32 r_c.root_reference) (b32 r_c.root_key) urn dict in
  pretty_to_string json

let test_encode_json filename =
 (*
	Takes content, convergence-secret and block-size from filename and returns the urn of the encoded content
	*)
  let json = from_file filename in
  let content = json |> member "content" |> to_string |> Base32.decode_exn in
  let convergence_secret = json |> member "convergence-secret" |> to_string |> Base32.decode_exn in
  let block_size = json |> member "block-size" |> to_int in
  let _dict, _r_c, urn = Eris.encode_urn ~convergence_secret ~block_size content in
  urn

let test_decode_json filename = 
 (*
	Takes blocks, block-size, level, root-reference, root-key from filename and return the Base32-encoded value of the decoded content
	*)
  let json = from_file filename in
  let dict = get_dict_from_json json in
  let block_size = json |> member "read-capability" |> member "block-size" |> to_int in
  let level = json |> member "read-capability" |> member "level" |> to_int in
  let root_reference = Base32.decode_exn (json |> member "read-capability" |> member "root-reference" |> to_string) in
  let root_key = Base32.decode_exn (json |> member "read-capability" |> member "root-key" |> to_string) in
  let r_c : Eris.ReadCapability.t = {block_size; level; root_reference; root_key} in
  Base32.encode_string ~pad:false (Eris.decode r_c dict)

let test_encode () =
  Alcotest.(
    for i = 0 to 12 do
      if i < 10 then
        let filename = "../../../../ocaml-eris/test-vectors/eris-test-vector-0"^(Int.to_string i)^".json" in
        check string ("Encode "^(Int.to_string i)) (test_encode_json (filename)) ((from_file (filename)) |> member "urn" |> to_string)
      else
        let filename = "../../../../ocaml-eris/test-vectors/eris-test-vector-"^(Int.to_string i)^".json" in
        check string ("Encode "^(Int.to_string i)) (test_encode_json (filename)) ((from_file (filename)) |> member "urn" |> to_string)
    done;
  )

let test_decode () =
  Alcotest.(
    for i = 0 to 12 do 
      if i < 10 then
        let filename = "../../../../ocaml-eris/test-vectors/eris-test-vector-0"^(Int.to_string i)^".json" in
        check string ("Decode "^(Int.to_string i)) (test_decode_json (filename)) ((from_file (filename)) |> member "content" |> to_string)
      else
        let filename = "../../../../ocaml-eris/test-vectors/eris-test-vector-"^(Int.to_string i)^".json" in
        check string ("Decode "^(Int.to_string i)) (test_decode_json (filename)) ((from_file (filename)) |> member "content" |> to_string)
    done;
  )

let () =
  let open Alcotest in
  run "Eris" [
    "encode", [
      test_case "from json" `Quick test_encode;
    ];
    "decode", [
      test_case "from json" `Quick test_decode;
    ]
  ]
