## NEEDED
Install Dune and Ocaml:
apt install opam
opam install dune
Make sure that dune is >= 2.1 with
dune --version
run: dune exec bin/main.exe



## Install Guix:

Install script: https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh

Copy to a .sh file and execute (You might need to run "wget 'https://sv.gnu.org/people/viewgpg.php?user_id=15145' -qO - | sudo -i gpg --import -" to setup gpg)

`guix pull for update`

## Define Guix environment:
`guix environment -l guix.scm`

### Update the guix environment (add a library from another gitlab repository):

To add a library to the guix environment, add this to the guix.scm file:

```
(define-public ocaml-base32
  (package
    (name "ocaml-base32")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-base32")
                     (commit "9fea35307723e78a4fa68952df583f4541e40b00")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0qhz4j5cply8kg83plhslh15sc2mbxkn40mbcbx272b0kcc6gxxq"))))
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     \`(("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-base32")
    (synopsis "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (description "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (license license:agpl3+)))
```
    
CHANGE "OCAML-BASE32" to "library", or something like this

You can get the hash using `guix download repository-url`

You also need to add 
```
("ocaml-base32" ,ocaml-base32)
```
to the `define-public/package/propagated-inputs` part.

After that, you need to update the dune file with, for example:
```
(library
	(name lib)
	(libraries monocypher base32))
```


#### Build:
dune build

#### Run ERIS:
`dune exec bin/main.exe`








##### Notes that should be in another file:

- How to use a library in the ocaml interpreter: (from https://stackoverflow.com/questions/8089023/launch-interactive-ocaml-session-with-library-yojson-available)

Run `eval $(opam env)` to set the environment
The tool you are after is called findlib. It is included in the base GODI installation. The tools that come with findlib allow you to easily compile against most OCaml libraries and use those libraries from a toplevel session (ocaml). The findlib documentation is fairly comprehensive, but here is a quick summary to get started.
To start using findlib from within a toplevel session:
#use "topfind";;
This will display a brief usage message. Then you can type:
#list;;
This will show you a list of all of the available packages. Yojson will likely be among them. Finally:
#require "yojson";;
where yojson is replaced by the appropriate entry shown by #list;;. Yojson's modules should be available for you to use at this point.


- ocp-indent
Can be integrated in Vim of Emacs https://www.typerex.org/ocp-indent.html
Can also be ran a command-line tool:
```
ocp-indent --inplace /home/nemael/Desktop/ocaml-eris/*/*.ml*
```



## NOT NEEDED
Install Nix (from https://www.tecmint.com/nix-package-manager-for-linux/)

sh <(curl https://nixos.org/nix/install) --daemon // Installation script
source /etc/profile.d/nix.sh
nix-shell -p nix-info --run "nix-info -m"

Add a nix channel:
nix-channel --add ... https://nixos.org/channels/nixpkgs-unstable

Add a nix package (local package?)
nix-env -i ... //apache-tomcat-9.0.2

## NOT NEEDED
Install Nix Flakes:
On non-nixos systems, install `nixUnstable` in your environment:
$ nix-env -iA nixpkgs.nixFlakes
Edit either ~/.config/nix/nix.conf or /etc/nix/nix.conf and add:
experimental-features = nix-command flakes
