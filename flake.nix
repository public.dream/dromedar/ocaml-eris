{
  description = "Ocaml implementation of Eris";

  outputs = { self, nixpkgs }:
    let
      systems = [ "aarch64-linux" "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs systems;
    in {

      overlay = final: prev: {
        ocamlPackages = prev.ocamlPackages.overrideScope' (final': prev':
          with final'; {

            base32 = buildDunePackage rec {
              pname = "base32";
              version = "0.0.0";
              useDune2 = true;
              src = final.fetchgit {
                url = "https://gitlab.com/public.dream/dromedar/ocaml-base32";
                sha256 = "sha256-iy2Rll5APilty8HZ164sYIgCVpMm/Rf/EK7Op0bW1kA=";
              };
            };

            eris = buildDunePackage rec {
              pname = "eris";
              version = "0.0.0";
              useDune2 = true;
              src = "${self}/lib";
              propagatedBuildInputs = [ monocypher yojson ];
            };

            monocypher = buildDunePackage rec {
              pname = "monocypher";
              version = "0.0.0";
              useDune2 = true;
              src = final.fetchgit {
                url =
                  "https://gitlab.com/public.dream/dromedar/ocaml-monocypher";
                sha256 = "sha256-diOrfMWWPFsyunny40R44mBbHiVK2OubI5WWW70V4ao=";
              };
              propagatedBuildInputs = [ base32 ctypes integers ];
            };

          });

      };

      legacyPackages = forAllSystems
        (system: nixpkgs.legacyPackages.${system}.extend self.overlay);

      packages = forAllSystems (system: {
        inherit (self.legacyPackages.${system}.ocamlPackages)
          base32 eris monocypher;
      });

      defaultPackage = forAllSystems (system: self.packages.${system}.eris);

    };
}
