module Block_size = struct
  let encoding_1k = 10 (* equivalent to '\0x0a', 2**10 = 1024 *)
  let encoding_32k = 15 (* equivalent to '\0x0f', 2**15 = 32768 *)
end

module Dict = struct
  let empty = []
  let insert key value dict = (key, value)::dict
  let lookup key dict = List.assoc key dict
  let combine dict1 dict2 = dict1 @ dict2
end

module ReadCapability = struct
  type t = {block_size:int; level:int; root_reference:string; root_key:string}
  let create_t_from_string string =
    let block_size = int_of_string (String.sub string 0 1) in 
    let level = int_of_string (String.sub string 1 1) in
    let root_reference = String.sub string 2 32 in
    let root_key = String.sub string 34 32 in
    {block_size; level; root_reference; root_key}
  let of_string string =
    create_t_from_string string
  let to_string r_c = 
    if r_c.block_size = 1024 then
      "urn:erisx2:" ^ Base32.encode_exn ~pad:false ((String.make 1 (Char.chr (Block_size.encoding_1k))) ^ (String.make 1 (Char.chr r_c.level)) ^ r_c.root_reference ^ r_c.root_key)
    else if r_c.block_size = 32768 then
      "urn:erisx2:" ^ Base32.encode_exn ~pad:false ((String.make 1 (Char.chr (Block_size.encoding_32k))) ^ (String.make 1 (Char.chr r_c.level)) ^ r_c.root_reference ^ r_c.root_key)
    else
      raise (Invalid_argument "Block size value can only be 1024 or 32768")
		
end

type blocks = (string * string) list


let rec length list =
  match list with
    [] -> 0
  |	_::t -> 1 + length t

let rec take n list =
  match list with
    [] -> []
  |	h::t ->
    if n < 0 then raise (Invalid_argument "n cannot be negative") else
    if n = 0 then [] else h::take (n - 1) t

let rec drop n list =
  match list with
    [] -> []
  |	_::t ->
    if n < 0 then raise (Invalid_argument "n cannot be negative") else
    if n = 0 then
      list
    else drop (n - 1) t

let first list =
  match list with
    [] -> raise (Invalid_argument "first") (* Change to Not_found *)
  |	h::_ -> h

let second list =
  match list with
    [] -> raise (Invalid_argument "second")
  |	_::t -> first t

let concat list =
  String.concat "" list

let rec concat_2_lvl list =
  match list with
    [] -> ""
  |	h::t -> String.concat "" [concat h; concat_2_lvl t]

let rec split_string string n =
  (* Should verify if the string has an even number of elements *)
  if (n) < String.length string then
    (String.sub string 0 (n)) :: split_string (String.sub string (n) (String.length string - (n))) n
  else [string]

let create_padding n =
  (String.make 1 (Char.chr 128))^(String.make (n-1) (Char.chr 0))

let pad string block_size =
  if ((String.length string) mod block_size) > 0 then
    string^(create_padding (block_size - ((String.length string) mod block_size)))
  else
    string^(create_padding block_size)

let rec unpad string block_size =
  let test_char = string.[(String.length string) - 1] in
  if test_char = Char.chr 0 then
    unpad (String.sub string 0 ((String.length string) - 1)) (block_size-1)
  else if test_char = Char.chr 128 then
    String.sub string 0 ((String.length string) - 1)
  else if block_size = -2 then
    raise (Invalid_argument "Content is not padded, no character '0x80' signifying the beginning of padding have been found in the last block_size -2 of the content")
  else
    raise (Invalid_argument "Content is not padded, data have been found after character '0x80' signifying the beginning of padding.")



(*
Cyptographic encoding of the blocs
*)

let encrypt_block input convergence_secret =
  let key = Monocypher.Hashing.Blake2b.digest ~size:32 ~key:convergence_secret input in
  let encrypted_block = Monocypher.Advanced.IETF_ChaCha20.crypt ~key ~nonce:(String.make 24 (Char.chr 0)) input in
  let reference = Monocypher.Hashing.Blake2b.digest ~size:32 encrypted_block in
  encrypted_block, reference, key

let rec fill_with_null_rk_pairs rk_pairs length_expected =
  if length rk_pairs < length_expected then
    fill_with_null_rk_pairs (rk_pairs @ [[String.make 32 (Char.chr 0); (String.make 32 (Char.chr 0))]]) length_expected
  else rk_pairs


let collect_rk_pairs input_rk_pairs convergence_secret block_size =
  let rk_pairs = ref [] in
  rk_pairs := !rk_pairs @ input_rk_pairs;
  let dict = ref Dict.empty in
  let arity = block_size/64 in
  let output_rk_pairs = ref [] in
  while (List.length !rk_pairs) > 0 do
    let rk_pairs_for_node = take arity !rk_pairs in (* takes arity rk pairs at a time *)
    rk_pairs := drop arity !rk_pairs;
    let rk_pairs_for_node = fill_with_null_rk_pairs rk_pairs_for_node arity in
    let node = concat_2_lvl rk_pairs_for_node in (* Node is constructed of r-k-r-k...*)
    let block, reference, key = encrypt_block node convergence_secret in
    dict := Dict.insert reference block !dict;
    let rk_to_node = [reference; key] in
    output_rk_pairs := !output_rk_pairs @ [rk_to_node]
  done;
  !output_rk_pairs, !dict



let block_storage_get reference dict =
  Dict.lookup reference dict

(*
Builds reference tree
*)

let split_content content convergence_secret block_size =
  let dict = ref Dict.empty in
  let rk_pairs = ref [] in
  let content = pad content block_size in
  let content = ref (split_string content block_size) in
  while length !content > 0 do
    (* The if length padded > block_size is not needed because I pad before, and padding won't be longer than expected *)
    let content_block = first !content in
    content := drop 1 !content;
    let encrypted_block, reference, key = encrypt_block content_block convergence_secret in
    dict := Dict.insert reference encrypted_block !dict;		
    let rk_pair = [reference; key] in
    rk_pairs := !rk_pairs @ [rk_pair];
  done;
  !rk_pairs, !dict

let split_node_to_rk_pairs string =
  if String.length string mod 64 == 0 then
    let res = ref [] in
    for i = 0 to ((String.length string)/64)-1 do
      let index = i*64 in
      let reference = String.sub string index 32 in
      let key = String.sub string (index+32) 32 in
      if not (reference = (String.make 32 (Char.chr 0)) || key = (String.make 32 (Char.chr 0))) then
        (
          res := !res @ [[reference; key]]
        )
    done;
    !res
  else
    raise (Invalid_argument "Length of rk_pair string number of characters is not a multiple of 64")


let create_urn read_capabilities =
  ReadCapability.to_string read_capabilities

let encode ?(convergence_secret = (String.make 32 (Char.chr 0))) ?(block_size = 1024) content =
  if not (block_size = 1024 || block_size = 32768) then
    raise (Invalid_argument "the block size can only be of 1024 bits or 32768 bits");
  let dict = ref Dict.empty in
  let level = ref 0 in (* is a ref to an int to make it mutable because it has to be updated in the following loop *)
  let rk_pairs = ref [] in (* is a ref to a list because it has to be updated in the following loop *)
  let rk_pairs_from_tuple, _dict_content = split_content content convergence_secret block_size in
  dict := Dict.combine !dict _dict_content;
  rk_pairs := !rk_pairs @ rk_pairs_from_tuple;
  while List.length(!rk_pairs) > 1 do
    let rk_pairs_from_tuple, _dict_rk_pairs = collect_rk_pairs !rk_pairs convergence_secret block_size in
    dict := Dict.combine !dict _dict_rk_pairs;
    rk_pairs := rk_pairs_from_tuple;
    level := !level + 1;
  done;
  let root_rk_pair = first !rk_pairs in
  let root_reference, root_key = first root_rk_pair, second root_rk_pair in
  let read_capabilities : ReadCapability.t = { block_size = block_size; level = !level; root_reference = root_reference; root_key = root_key } in
  !dict, read_capabilities

let encode_urn ?(convergence_secret = (String.make 32 (Char.chr 0))) ?(block_size = 1024) content = 
  let dict, r_c = encode ~convergence_secret ~block_size content in
  let urn = create_urn r_c in
  dict, r_c, urn

let rec eris_decode_recurse dict level reference key =
  if level == 0 then
    (
      let encrypted_content_block = block_storage_get reference dict in
      let res = Monocypher.Advanced.IETF_ChaCha20.crypt ~key ~nonce:(String.make 24 (Char.chr 0)) encrypted_content_block in (* returns the unencrypted content block *)
      res
    )
  else
    (
      let encrypted_node = block_storage_get reference dict in
      let node = Monocypher.Advanced.IETF_ChaCha20.crypt ~key ~nonce:(String.make 24 (Char.chr 0)) encrypted_node in (* node is a list of rk-pair (which is a tuple) *)
      let node = ref (split_node_to_rk_pairs node) in
      let output = ref [] in
      while List.length !node > 0 do
        let sub_rk_pair = first !node in
        let sub_reference = first sub_rk_pair in
        let sub_key = second sub_rk_pair in
        node := drop 1 !node;
        let res = eris_decode_recurse dict (level-1) sub_reference sub_key in
        output := !output @ [res]
      done;
      concat !output
    )


let decode (r_c:ReadCapability.t) dict =
  let res = eris_decode_recurse dict r_c.level r_c.root_reference r_c.root_key in
  unpad (concat[res]) r_c.block_size
