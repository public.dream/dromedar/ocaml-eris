module ReadCapability : sig
  type t = {block_size:int; level:int; root_reference:string; root_key:string}
  val to_string : t -> string
  val of_string : string -> t
end

type blocks = (string * string) list

(* Eris module interface *)

val encode : ?convergence_secret:string -> ?block_size:int -> string -> blocks * ReadCapability.t

(**	Encodes a string of content with its convergence secret, following a block_size
	-Parameters:
		convergence_secret: String, it is the convergence secret used to encode the content.
		block_size: Int, it is the size of the blocks used to encode the content, it can only be 1024 or 32768
		content: String, it is the content to be encoded
	-Returns:
		blocks: (string*string list) A dictionnary containing references and blocks. The references are Blake2b hashes of their blocks.
		block_size: Int, the block size used to encode the content
		level: Int, the level of the tree used to encode the content
		root_reference: String, the reference to access root node
		root_key: String, the key to decode the root node
*)

val encode_urn : ?convergence_secret:string -> ?block_size:int -> string -> (string * string) list * ReadCapability.t * string
(** Encodes a string of content with its convergence secret, following a block_size, and return its URN
    	-Parameters:
    		Same parameters as function encode
    	-Returns:
    		Same returns as function encode
    		urn: String, is a string containing the URN of the encoded content (Its read-capabilities)
*)


val decode : ReadCapability.t -> blocks -> string
(** Decode content that has been encoded with Eris
	-Parameters:
		ReadCapability.t: Is a type t from the module ReadCapability. It contains a block_size, a level, a root_reference and a root_key
		blocks: is a (string*string) list, the dicionnary containing the encoded blocks by Eris

	-Returns
		res: the string of the decoded content
*)
