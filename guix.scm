(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system dune)
 ((guix licenses) #:prefix license:)
 (gnu packages ocaml))

(define-public ocaml-monocypher
  (package
    (name "ocaml-monocypher")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-monocypher")
                     (commit "e174fd6040b920c47c69628725aec9e27999e541")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ap12nymp5lm4fdypn2a4lg5nq72g12f7wkrp8r5ng4nqmyan8vn"))))
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     `(("ocaml-integers" ,ocaml-integers)
       ("ocaml-ctypes" ,ocaml-ctypes)))
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-monocypher")
    (synopsis "OCaml bindings to the Monocypher cryptographic library")
    (description "sdfsdf")
    (license license:agpl3+)))

(define-public ocaml-base32
  (package
    (name "ocaml-base32")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-base32")
                     (commit "9bbe2a1fdb2ac3905b836aceb45336883baa2bf0")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0h6nsr3agkmf23zigz96jdb052305jpdgnf1rdnjjgj0bsb92bcb"))))
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     `(("qcheck" ,ocaml-qcheck)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-base32")
    (synopsis "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (description "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (license license:agpl3+)))

(define-public ocaml-eris
  (package
    (name "ocaml-eris")
    (version "0.0.0")
    (source #f)
    (build-system dune-build-system)
    (arguments '())
    (native-inputs '())
    (propagated-inputs
     `(("ocaml-monocypher" ,ocaml-monocypher)
	   ("ocaml-base32" ,ocaml-base32)
	   ("ocaml-yojson" ,ocaml-yojson)
	   ("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-eris")
    (synopsis "OCaml implementation of ERIS")
    (description "sdfsdf")
    (license license:agpl3+)))

ocaml-eris
