## Dromedar-eris

This is an Ocaml implementation of [Eris](https://openengiadina.gitlab.io/eris/)

(WIP)

## Usage

##### You can run tests with
```
dune runtest
```
This will execute automatic tests found in test/test.ml

##### Add to guix
You can add the library to guix by adding the following snippet
``` scheme
(define-public ocaml-eris
  (package
    (name "ocaml-eris")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-eris")
                     (commit "4968338ddc8b697abc0f09b202b8d26cc284d073")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0m646ivslm3xx9yb5xa1ksvpk0a8knjd2y8jrsjpzn91lhprsk34"))))
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)))
    (propagated-inputs
     `(("ocaml-monocypher" ,ocaml-monocypher)
       ("ocaml-base32" ,ocaml-base32)
       ("ocaml-yojson" ,ocaml-yojson) ("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-eris")
    (synopsis "OCaml implementation of ERIS")
    (description "Ocaml implementation of ERIS, see homepage for a link to ERIS design document")
    (license license:agpl3+)))
```

(The 'commmit' and 'sha256' fields might need to be updated accroding to the most recent Eris commit)

#### Disclaimer:
The test-vectors are provided by [Pukamustard](https://gitlab.com/pukkamustard) from his [Scheme Eris implementation](https://gitlab.com/openengiadina/eris/)
